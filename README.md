# Paybutton Integration Examples #

Simple sample projects that integrate the Paybutton 2.0, both in Kotlin as in Java.
The projects depend on a local snapshot build of the paybutton library.

### How to test the integration with a maven local snapshot build? ###

* Set `publish.snapshot=true` into the io.payworks.mpos.sdk repository.
* Run `./gradlew publishToMavenLocalObfuscated` in the io.payworks.mpos.sdk root folder.
* Update the paybutton snapshot dependecy version number in the sample project module's build.gradle.
