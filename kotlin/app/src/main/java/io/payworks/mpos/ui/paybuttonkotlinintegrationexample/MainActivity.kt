package io.payworks.mpos.ui.paybuttonkotlinintegrationexample

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import io.mpos.accessories.AccessoryFamily
import io.mpos.accessories.parameters.AccessoryParameters
import io.mpos.paybutton.MposUi
import io.mpos.paybutton.UiConfiguration
import io.mpos.paybutton.UiConfiguration.*
import io.mpos.provider.ProviderMode
import io.mpos.transactions.Currency
import io.mpos.transactions.TransactionWorkflowType
import io.mpos.transactions.parameters.TransactionParameters
import java.math.BigDecimal

class MainActivity : AppCompatActivity() {
    var mposUi: MposUi? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<View>(R.id.buttonStartTransaction).setOnClickListener {
            findViewById<View>(R.id.buttonStartTransaction).isEnabled = false
            startTransaction()
        }
        findViewById<View>(R.id.buttonShowSummary).setOnClickListener {
            findViewById<View>(R.id.buttonShowSummary).isEnabled = false
            showSummaryForTransaction()
        }
        setupPaybutton()
    }

    private fun setupPaybutton() {
        mposUi = MposUi.create(
                context = this,
                providerMode = ProviderMode.MOCK,  // replace with the environment of your merchant
                merchantId = "f618cb42-8665-11eb-8dcd-0242ac130003",  // replace with your merchant id
                merchantSecret = "yourMerchantSecret") // replace with your secret key

        // Setup configuration
        val configuration = UiConfiguration(
                summaryFeatures = setOf(
                        SummaryFeature.CAPTURE_TRANSACTION,
                        SummaryFeature.PRINT_CUSTOMER_RECEIPT,
                        SummaryFeature.PRINT_MERCHANT_RECEIPT,
                        SummaryFeature.REFUND_TRANSACTION,
                        SummaryFeature.SEND_RECEIPT_VIA_EMAIL
                ),
                paymentOptions = setOf(PaymentOption.CARD, PaymentOption.MOTO),
                signatureCapture = SignatureCapture.ON_SCREEN,
                resultDisplayBehavior = ResultDisplayBehavior.DISPLAY_INDEFINITELY,
                accessibilityModeOption = AccessibilityModeOption.OPTION_VISIBLE,
                terminalParameters = AccessoryParameters.Builder(AccessoryFamily.PAX)
                    .integrated()
                    .build()
        )
        mposUi?.configuration = configuration

        // Setup theme
        mposUi?.themeRes = R.style.Theme_AppTheme_SampleTheme
    }

    private fun startTransaction() {
        val transactionParameters = TransactionParameters.Builder()
                .charge(BigDecimal("1.00"), Currency.EUR)
                .subject("Bouquet of Flowers")
                .customIdentifier("yourReferenceForTheTransaction")
                .workflow(TransactionWorkflowType.UNKNOWN)
                .subject("Bouquet of Flowers")
                .metadata(
                        mapOf(Pair("clerk", "John Doe"), Pair("sale commission", "2%"))
                )
                .build()

        val transactionIntent = mposUi?.createTransactionIntent(transactionParameters)
        startActivityForResult(transactionIntent, MposUi.REQUEST_CODE_PAYMENT)
    }

    private fun showSummaryForTransaction() {
        val summaryIntent = mposUi?.createTransactionSummaryIntent("transactionIdentifier")
        startActivityForResult(summaryIntent, MposUi.REQUEST_CODE_SHOW_SUMMARY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            // Request code from a successful transaction
            MposUi.REQUEST_CODE_PAYMENT -> {

                when (resultCode) {
                    // Result code from a successful transaction
                    MposUi.RESULT_CODE_APPROVED -> {
                        val transactionIdentifier = data?.getStringExtra(MposUi.RESULT_EXTRA_TRANSACTION_IDENTIFIER)
                        Snackbar.make(findViewById(android.R.id.content),"Transaction approved!\nIdentifier: $transactionIdentifier", Snackbar.LENGTH_LONG).show()
                    }
                    // Result code from a declined, aborted or failed transaction
                    MposUi.RESULT_CODE_FAILED -> {
                        Snackbar.make(findViewById(android.R.id.content), "Transaction was declined, aborted, or failed", Snackbar.LENGTH_LONG).show()
                    }
                }

                findViewById<View>(R.id.buttonStartTransaction).isEnabled = true
            }
            // Request code from querying a transaction summary
            MposUi.REQUEST_CODE_SHOW_SUMMARY -> {
                findViewById<View>(R.id.buttonShowSummary).isEnabled = true
            }
        }
    }
}