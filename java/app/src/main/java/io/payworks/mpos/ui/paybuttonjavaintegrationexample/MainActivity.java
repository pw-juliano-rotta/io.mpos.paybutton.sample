package io.payworks.mpos.ui.paybuttonjavaintegrationexample;

import androidx.appcompat.app.AppCompatActivity;
import io.mpos.paybutton.MposUi;
import io.mpos.paybutton.UiConfiguration;
import io.mpos.provider.ProviderMode;
import io.mpos.transactions.TransactionWorkflowType;
import io.mpos.transactions.parameters.TransactionParameters;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.EnumSet;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    MposUi mposUi = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setupPaybutton();

        findViewById(R.id.buttonStartTransaction).setOnClickListener(view -> {
            findViewById(R.id.buttonStartTransaction).setEnabled(false);
            startTransaction();
        });

        findViewById(R.id.buttonShowSummary).setOnClickListener(view -> {
            findViewById(R.id.buttonShowSummary).setEnabled(false);
            showSummaryForTransaction();
        });
    }

    private void setupPaybutton() {
        mposUi = MposUi.create(
                this,
                ProviderMode.MOCK, // replace with the environment of your merchant
                "f618cb42-8665-11eb-8dcd-0242ac130003", // replace with your merchant id
                "yourMerchantSecret"); // replace with your secret key

        // Setup configuration
        UiConfiguration configuration = new UiConfiguration.Builder()
                .paymentOptions(
                        EnumSet.of(
                            UiConfiguration.PaymentOption.CARD,
                            UiConfiguration.PaymentOption.MOTO
                        )
                )
                .signatureCapture(UiConfiguration.SignatureCapture.ON_RECEIPT)
                .resultDisplayBehavior(UiConfiguration.ResultDisplayBehavior.DISPLAY_INDEFINITELY)
                .accessibilityModeOption(UiConfiguration.AccessibilityModeOption.OPTION_VISIBLE)
                .summaryFeatures(
                        EnumSet.of(
                            UiConfiguration.SummaryFeature.SEND_RECEIPT_VIA_EMAIL,
                            UiConfiguration.SummaryFeature.PRINT_CUSTOMER_RECEIPT,
                            UiConfiguration.SummaryFeature.PRINT_MERCHANT_RECEIPT,
                            UiConfiguration.SummaryFeature.REFUND_TRANSACTION,
                            UiConfiguration.SummaryFeature.CAPTURE_TRANSACTION
                        )
                )
                .build();
        mposUi.setConfiguration(configuration);
        
        // Setup theme
        mposUi.setThemeRes(R.style.Theme_AppTheme_SampleTheme);
    }

    private void startTransaction() {
        TransactionParameters transactionParameters = new TransactionParameters.Builder()
                .charge(new BigDecimal("1.00"), io.mpos.transactions.Currency.EUR)
                .subject("Bouquet of Flowers")
                .customIdentifier("yourReferenceForTheTransaction")
                .workflow(TransactionWorkflowType.UNKNOWN)
                .subject("Bouquet of Flowers")
                .metadata(
                        new HashMap<String, String>() {{
                            put("clerk", "John Doe");
                            put("sale commission", "2%");
                        }}
                )
                .build();

        Intent transactionIntent = mposUi.createTransactionIntent(transactionParameters);
        startActivityForResult(transactionIntent, MposUi.REQUEST_CODE_PAYMENT);
    }

    private void showSummaryForTransaction() {
        Intent summaryIntent = mposUi.createTransactionSummaryIntent("transactionIdentifier");
        startActivityForResult(summaryIntent, MposUi.REQUEST_CODE_SHOW_SUMMARY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MposUi.REQUEST_CODE_PAYMENT) {
            if (resultCode == MposUi.RESULT_CODE_APPROVED) {
                // Transaction was approved
                String transactionIdentifier = data.getStringExtra(MposUi.RESULT_EXTRA_TRANSACTION_IDENTIFIER);
                Toast.makeText(this, "Transaction approved, identifier: " + transactionIdentifier, Toast.LENGTH_LONG).show();
            } else {
                // Card was declined, or transaction was aborted, or failed
                // (e.g. no internet or accessory not found)
                Toast.makeText(this, "Transaction was declined, aborted, or failed",
                        Toast.LENGTH_LONG).show();
            }

            findViewById(R.id.buttonStartTransaction).setEnabled(true);
        } else if (requestCode == MposUi.REQUEST_CODE_SHOW_SUMMARY) {
            findViewById(R.id.buttonShowSummary).setEnabled(true);
        }
    }
}