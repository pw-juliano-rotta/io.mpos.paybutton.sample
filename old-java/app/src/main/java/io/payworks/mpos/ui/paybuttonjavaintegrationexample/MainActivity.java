package io.payworks.mpos.ui.paybuttonjavaintegrationexample;

import androidx.appcompat.app.AppCompatActivity;

import io.mpos.accessories.AccessoryFamily;
import io.mpos.accessories.parameters.AccessoryParameters;
import io.mpos.provider.ProviderMode;
import io.mpos.transactions.parameters.TransactionParameters;
import io.mpos.ui.shared.MposUi;
import io.mpos.ui.shared.model.MposUiConfiguration;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {

    MposUi mposUi = null;
    String lastTransactionIdentifier = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setupPaybutton();

        findViewById(R.id.buttonStartTransaction).setOnClickListener(view -> {
            findViewById(R.id.buttonStartTransaction).setEnabled(false);
            startTransaction();
        });

        findViewById(R.id.buttonShowSummary).setOnClickListener(view -> {
            findViewById(R.id.buttonShowSummary).setEnabled(false);
            showSummaryForTransaction();
        });
    }

    private void setupPaybutton() {
        mposUi = MposUi.initialize(
                this,
                ProviderMode.TEST, // replace with the environment of your merchant
                "a1e105f9-d053-4e9b-a8f9-344d28de3ee9", // replace with your merchant id
                "lxUvBEAWW8MkyGjYorjCd5CYjqP2DtlE"); // replace with your secret key

        AccessoryParameters accessoryParameters = new AccessoryParameters.Builder(AccessoryFamily.PAX)
                .integrated()
                .build();

        mposUi.getConfiguration().setAccessibilityModeOption(MposUiConfiguration.AccessibilityModeOption.OPTION_VISIBLE);
        mposUi.getConfiguration().setTerminalParameters(accessoryParameters);
        mposUi.getConfiguration().setPrinterParameters(accessoryParameters);
    }

    private void startTransaction() {
        TransactionParameters transactionParameters = new TransactionParameters.Builder()
                .charge(new BigDecimal("5.00"), io.mpos.transactions.Currency.EUR)
                .subject("Bouquet of Flowers")
                .customIdentifier("yourReferenceForTheTransaction")
                .build();

        Intent intent = mposUi.createTransactionIntent(transactionParameters);
        startActivityForResult(intent, MposUi.REQUEST_CODE_PAYMENT);
    }

    private void showSummaryForTransaction() {
        Intent summaryIntent = mposUi.createTransactionSummaryIntent(lastTransactionIdentifier);
        startActivityForResult(summaryIntent, MposUi.REQUEST_CODE_SHOW_SUMMARY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MposUi.REQUEST_CODE_PAYMENT) {
            if (resultCode == MposUi.RESULT_CODE_APPROVED) {
                // Transaction was approved
                lastTransactionIdentifier = mposUi.getTransaction().getIdentifier();
                Toast.makeText(this, "Transaction approved, identifier: " + lastTransactionIdentifier, Toast.LENGTH_LONG).show();
            } else {
                // Card was declined, or transaction was aborted, or failed
                // (e.g. no internet or accessory not found)
                Toast.makeText(this, "Transaction was declined, aborted, or failed",
                        Toast.LENGTH_LONG).show();
            }

            findViewById(R.id.buttonStartTransaction).setEnabled(true);
        } else if (requestCode == MposUi.REQUEST_CODE_SHOW_SUMMARY) {
            findViewById(R.id.buttonShowSummary).setEnabled(true);
        }
    }
}